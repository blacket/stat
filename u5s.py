import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

pd.set_option('display.width', 320)
pd.set_option('display.max_columns', 20)
pd.set_option('display.max_colwidth', 160)
# pd.set_option('display.precision', 2)

date = '2022-09-01'

dir = 'd/data/spot/daily/aggTrades/ETHUSDT'
file1 = f'{dir}/ETHUSDT-aggTrades-{date}.5s.csv'

df = pd.read_csv(file1,
                 nrows=200,
                 index_col='ts',
                 )

print(df.head())

df.index = pd.PeriodIndex(data=df.index, freq='5S')

# ss['pct'] = ss['price'].pct_change()
df['ma_5m'] = df['open'].rolling('5min', min_periods=12).mean()
df['ema_5m'] = df['open'].ewm(60, min_periods=12).mean()

print(df.head())

df[['open', 'ma_1m', 'ma_5m', 'ema_5m']].plot()

plt.show()
