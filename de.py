import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

pd.set_option('display.width', 320)
pd.set_option('display.max_columns', 20)
pd.set_option('display.max_colwidth', 160)
pd.set_option('display.precision', 2)

date = '2022-09-01'

dir = 'd/data/spot/daily/aggTrades/ETHUSDT'
file1 = f'{dir}/ETHUSDT-aggTrades-{date}.csv'

df = pd.read_csv(file1,
                 # nrows=200,
                 header=None,
                 names=["tradeId", "price", "quantity", "First tradeId", "Last tradeId", "ts",
                        "buyer the maker", "best price match"],
                 usecols=["ts", "price", "quantity"],
                 index_col='ts')

print(df.head())

df.index = df.index.map(lambda t: pd.Timestamp(t * 1_000_000))
print(df.head())

df.index = pd.PeriodIndex(data=df.index, freq='S')
df['volume'] = df['price'] * df['quantity']
print(df.head())

dfs = df.resample('S')
ohlc = dfs['price'].ohlc()
print(ohlc)

ss = dfs.agg({'quantity': 'sum', 'volume': 'sum'})

ss['price'] = ss['volume'] / ss['quantity']

ss[ohlc.columns] = ohlc
print(ss.head())

# ss.plot()
# plt.show()

to_file1 = f'{dir}/ETHUSDT-aggTrades-{date}.1s.csv'
ss.to_csv(to_file1)
