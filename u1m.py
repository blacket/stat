import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

pd.set_option('display.width', 320)
pd.set_option('display.max_columns', 20)
pd.set_option('display.max_colwidth', 160)
# pd.set_option('display.precision', 2)

date = '2022-09-05'

base_dir = 'd/data/spot/daily/klines/ETHUSDT/1m'
file1 = f'{base_dir}/ETHUSDT-{date}.1m.csv'

df = pd.read_csv(file1,
                 nrows=200,
                 index_col='ts',
                 )

print(df.head())

df.index = pd.PeriodIndex(data=df.index, freq='1min')

df['year'] = df.index.year
df['month'] = df.index.month
df['day'] = df.index.day
df['day_of_week'] = df.index.day_of_week
df['hour'] = df.index.hour
df['minute'] = df.index.minute
df['second'] = df.index.second

# ss['pct'] = ss['price'].pct_change()
# df['ma_1h'] = df['open'].rolling('1H', min_periods=10).mean()
# df['ema_1h'] = df['open'].ewm(60, min_periods=10).mean()

print(df.head())

# df[['open', 'ma_1h', 'ema_1h']].plot()
# plt.show()
