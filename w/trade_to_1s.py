from pd_opt import *

month = '2022-10'

dir = '../d/data/spot/monthly/aggTrades/ETHUSDT'
file1 = f'{dir}/ETHUSDT-aggTrades-{month}.csv'

df = pd.read_csv(file1,
                 # nrows=200,
                 header=None,
                 names=["tradeId", "price", "quantity", "firstTradeId", "lastTradeId", "ts",
                        "buyerTheMaker", "bestPriceMatch"],
                 usecols=["ts", "price", "quantity", "buyerTheMaker"],
                 index_col='ts')

df.index = df.index.map(lambda t: pd.Timestamp(t * 1_000_000))
df.index = pd.PeriodIndex(data=df.index, freq='S')
df['volume'] = df['price'] * df['quantity']
df['buyQuantity'] = df['quantity'] * df['buyerTheMaker']
df['sellQuantity'] = df['quantity'] * ~ df['buyerTheMaker']
# print(df.head())

dfs = df.resample('S')
ohlc = dfs['price'].ohlc()
aggRows = dfs['price'].count()
# print(ohlc)
# print(aggRows)

ss = dfs.agg({'volume': 'sum', 'quantity': 'sum', 'buyQuantity': 'sum', 'sellQuantity': 'sum'})
ss[ohlc.columns] = ohlc
ss['aggRows'] = aggRows

next_open = ss['open']
next_open.index = next_open.index.shift(-1)
ss['next_open'] = next_open

print(ss.head())
to_file1 = f'{dir}/ETHUSDT-aggTrades-{month}.1s.csv'
ss.to_csv(to_file1)
