
import pandas as pd

pd.set_option('display.width', 320)
pd.set_option('display.max_columns', 20)
pd.set_option('display.max_colwidth', 160)
pd.set_option('display.precision', 2)
