from pd_opt import *
import pandas as pd

month = '2022-10'

dir = '../d/data/spot/monthly/aggTrades/ETHUSDT'
file1 = f'{dir}/ETHUSDT-aggTrades-{month}.5s.csv'

df = pd.read_csv(file1,
                 # nrows=200,
                 index_col='ts',
                 )
print(df.head())

df.index = pd.PeriodIndex(data=df.index, freq='5S')

dfs = df.resample('30S')

ss = dfs.agg({'volume': 'sum',
              'quantity': 'sum',
              'buyQuantity': 'sum',
              'sellQuantity': 'sum',
              'open': 'first',
              'high': 'max',
              'low': 'min',
              'close': 'last',
              'aggRows': 'sum'})

next_open = ss['open']
next_open.index = next_open.index.shift(-1)
ss['next_open'] = next_open

print(ss.head())

to_file1 = f'{dir}/ETHUSDT-aggTrades-{month}.30s.csv'
ss.to_csv(to_file1
          # , float_format='%.4f'
          )
