import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

pd.set_option('display.width', 320)
pd.set_option('display.max_columns', 20)
pd.set_option('display.max_colwidth', 160)
pd.set_option('display.precision', 2)

date = '2022-09-05'

dir = 'd/data/spot/daily/klines/ETHUSDT/1m'
file1 = f'{dir}/ETHUSDT-1m-{date}.csv'

df = pd.read_csv(file1,
                 header=None,
                 names=["ts", "open", "high", "low", "close", "quantity", "close_ts", "volume",
                        "trades", "tbbav", "tbqav", "igno"],
                 usecols=["ts", "open", "high", "low", "close", "quantity", "volume"],
                 index_col='ts')

print(df.head())

df.index = df.index.map(lambda t: pd.Timestamp(t * 1_000_000))

df.index = pd.PeriodIndex(data=df.index, freq='1min')
print(df.head())

df['open'].plot()
plt.show()

to_file1 = f'{dir}/ETHUSDT-{date}.1m.csv'
df.to_csv(to_file1)
