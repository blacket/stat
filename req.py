import os, shutil
import requests

# r = requests.get('https://google.com')
# print(r.text)


def down_and_unzip():
    dir = 'data/spot/daily/klines/ETHUSDT/1m/'
    date = '2022-09-04'
    filename = f'ETHUSDT-1m-{date}.zip'
    target_file = f'd/{dir}{filename}'

    with requests.get(f'https://data.binance.vision/{dir}{filename}', stream=True) as r:
        r.raise_for_status()
        target_dir = os.path.dirname(target_file)
        if not os.path.exists(target_dir):
            os.makedirs(target_dir, exist_ok=True)
        with open(target_file, 'wb') as f:
            shutil.copyfileobj(r.raw, f)
    shutil.unpack_archive(target_file, f'd/{dir}')


down_and_unzip()
