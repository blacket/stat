import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

pd.set_option('display.width', 320)
pd.set_option('display.max_columns', 20)
pd.set_option('display.max_colwidth', 160)
# pd.set_option('display.precision', 2)

date = '2022-09-01'

dir = 'd/data/spot/daily/aggTrades/ETHUSDT'
file1 = f'{dir}/ETHUSDT-aggTrades-{date}.1s.csv'

df = pd.read_csv(file1,
                 # nrows=200,
                 index_col='ts',
                 )

print(df.head())

df.index = pd.PeriodIndex(data=df.index, freq='S')

dfs = df.resample('5S')

ss = dfs.agg({'quantity': 'sum',
              'volume': 'sum',
              'open': 'first',
              'high': 'max',
              'low': 'min',
              'close': 'last'})

ss['price'] = ss['volume'] / ss['quantity']

# ss['pct'] = ss['price'].pct_change()
ss['ma_1m'] = ss['open'].rolling('1min', min_periods=3).mean()

print(ss.head())

# ss[['price', 'open']].plot()

# ss.plot()
# plt.show()

to_file1 = f'{dir}/ETHUSDT-aggTrades-{date}.5s.csv'
ss.to_csv(to_file1
          # , float_format='%.4f'
          )
