import zipfile

dir = 'd/data/spot/daily/aggTrades/ETHUSDT'
with zipfile.ZipFile(dir + '/ETHUSDT-aggTrades-2022-09-02.zip', 'r') as zip_ref:
    zip_ref.extractall(dir)
