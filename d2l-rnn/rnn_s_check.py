import tensorflow as tf
from d2l import tensorflow as d2l


RNNScratch = d2l.RNNScratch
RNNLMScratch = d2l.RNNLMScratch


def check():
    batch_size, num_inputs, num_hiddens, num_steps = 2, 16, 32, 100
    rnn0 = RNNScratch(num_inputs, num_hiddens)
    X = tf.ones((num_steps, batch_size, num_inputs))
    outputs, state = rnn0(X)

    d2l.check_len(outputs, num_steps)
    d2l.check_shape(outputs[0], (batch_size, num_hiddens))
    d2l.check_shape(state, (batch_size, num_hiddens))

    model0 = RNNLMScratch(rnn0, num_inputs)
    outputs = model0(tf.ones((batch_size, num_steps), dtype=tf.int64))
    d2l.check_shape(outputs, (batch_size, num_steps, num_inputs))


check()
