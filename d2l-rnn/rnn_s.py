import sys
import os
sys.path.insert(1, os.path.join(sys.path[0], '..'))


from d2l import tensorflow as d2l


data = d2l.TimeMachine(batch_size=1024, num_steps=32)
with d2l.try_gpu():
    rnn = d2l.RNNScratch(num_inputs=len(data.vocab), num_hiddens=32)
    model = d2l.RNNLMScratch(rnn, vocab_size=len(data.vocab), lr=1)
trainer = d2l.Trainer(max_epochs=1, gradient_clip_val=1)
trainer.fit(model, data)

pr = model.predict('it has', 20, data.vocab)
print(pr)
